{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Language.GraphMach.Machine.Inst where

import Data.Kind
import Language.GraphMach.Machine.Value

data Inst (f :: Type -> Type) where
  Push :: Value -> Inst f
  MkApp :: Inst f
  Slide :: Int -> Inst f
  Cond :: f (Inst f) -> f (Inst f) -> Inst f
  Eval :: Inst f
  Unwind :: Inst f
  Add :: Inst f
  Sub :: Inst f
  Mul :: Inst f
  Div :: Inst f
  Equ :: Inst f
  NEq :: Inst f

deriving instance (Eq (f (Inst f))) => Eq (Inst f)

deriving instance (Show (f (Inst f))) => Show (Inst f)
