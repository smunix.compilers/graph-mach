{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Language.GraphMach.Machine.Stack where

import Language.GraphMach.Machine.Addr
import Optics

newtype Stack f where
  Stack :: {_runStack :: f Addr} -> Stack f

makeLenses ''Stack

deriving newtype instance (Eq (f Addr)) => Eq (Stack f)

deriving newtype instance (Ord (f Addr)) => Ord (Stack f)

deriving newtype instance (Show (f Addr)) => Show (Stack f)

deriving newtype instance (Semigroup (f Addr)) => Semigroup (Stack f)

deriving newtype instance (Monoid (f Addr)) => Monoid (Stack f)

instance (Eq (f Addr), Monoid (f Addr)) => AsEmpty (Stack f)

instance (Cons (f Addr) (f Addr) Addr Addr) => Cons (Stack f) (Stack f) Addr Addr where _Cons = prism' (\(i, Stack is) -> Stack (i :< is)) (\case (Stack (i :< is)) -> pure (i, Stack is); _ -> Nothing)

type instance IxValue (Stack f) = Addr

type instance Index (Stack f) = Int

instance (Index (f Addr) ~ Int, IxValue (f Addr) ~ Addr, At (f Addr)) => Ixed (Stack f)

instance
  ( At (f Addr),
    IxValue (f Addr) ~ Addr,
    Index (f Addr) ~ Int
  ) =>
  At (Stack f)
  where
  at n = lens getter setter
    where
      setter :: Stack f -> Maybe Addr -> Stack f
      setter = error "not implemented"

      getter :: Stack f -> Maybe Addr
      getter = view (runStack % at n)
