{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Language.GraphMach.Machine.Node where

import Language.GraphMach.Machine.Addr
import Language.GraphMach.Machine.Inst
import Optics

data Node f where
  App :: Addr -> Addr -> Node f
  Num :: Int -> Node f
  Fun :: String -> Int -> f (Inst f) -> Node f

makePrisms ''Node

deriving instance (Eq (f (Inst f))) => Eq (Node f)

deriving instance (Ord (f (Inst f))) => Ord (Node f)

deriving instance (Show (f (Inst f))) => Show (Node f)
