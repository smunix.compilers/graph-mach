{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- |
module Language.GraphMach.Machine.Addr where

import Data.Hashable
import Foreign (Storable)

newtype Addr where
  Addr :: Int -> Addr
  deriving newtype (Show, Ord, Eq, Num, Hashable, Storable)
