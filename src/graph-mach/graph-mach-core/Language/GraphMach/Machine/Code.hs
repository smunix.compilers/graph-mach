{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Language.GraphMach.Machine.Code where

import Language.GraphMach.Machine.Inst
import Optics

newtype Code f where
  Code :: {_runCode :: f (Inst f)} -> Code f

makeLenses ''Code

deriving newtype instance (Eq (f (Inst f))) => Eq (Code f)

deriving newtype instance (Ord (f (Inst f))) => Ord (Code f)

deriving newtype instance (Show (f (Inst f))) => Show (Code f)

deriving newtype instance (Semigroup (f (Inst f))) => Semigroup (Code f)

deriving newtype instance (Monoid (f (Inst f))) => Monoid (Code f)

instance (Eq (f (Inst f)), Monoid (f (Inst f))) => AsEmpty (Code f)

instance (Cons (f (Inst f)) (f (Inst f)) (Inst f) (Inst f)) => Cons (Code f) (Code f) (Inst f) (Inst f) where _Cons = prism' (\(i, Code is) -> Code (i :< is)) (\case (Code (i :< is)) -> pure (i, Code is); _ -> Nothing)

type instance IxValue (Code f) = (Inst f)

type instance Index (Code f) = Int

instance (Index (f (Inst f)) ~ Int, IxValue (f (Inst f)) ~ Inst f, At (f (Inst f))) => Ixed (Code f)

instance
  ( At (f (Inst f)),
    IxValue (f (Inst f)) ~ Inst f,
    Index (f (Inst f)) ~ Int
  ) =>
  At (Code f)
  where
  at n = lens getter setter
    where
      setter :: Code f -> Maybe (Inst f) -> Code f
      setter = error "not implemented"

      getter :: Code f -> Maybe (Inst f)
      getter = view (runCode % at n)
