{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Language.GraphMach.Machine.Machine where

import Control.Monad.State.Strict (MonadState (get, put))
import Data.HashMap.Strict
import Data.Hashable
import Data.Kind (Constraint, Type)
import Data.Vector (Vector)
import Foreign (Storable)
import GHC.TypeLits (Symbol)
import Language.GraphMach.Machine.Addr
import Language.GraphMach.Machine.Code
import Language.GraphMach.Machine.Inst
import Language.GraphMach.Machine.Node
import Language.GraphMach.Machine.Stack
import Language.GraphMach.Machine.Value
import Optics

data Machine f where
  Machine ::
    { _code :: Code f,
      _stack :: Stack f,
      _heap :: HashMap Addr (Node f),
      _dump :: f (Code f, Stack f),
      _globals :: HashMap String Addr
    } ->
    Machine f

makeLenses ''Machine

type Store :: (Type -> Type) -> Constraint

type Store f =
  ( Eq (f (Inst f)),
    Monoid (f (Inst f)),
    Cons (f Addr) (f Addr) Addr Addr,
    Cons (f (Inst f)) (f (Inst f)) (Inst f) (Inst f)
  )

type MonadMachine :: (Type -> Type) -> (Type -> Type) -> Constraint

type MonadMachine f m =
  ( Store f,
    Alloc
      "heap"
      f
      ( Node f ->
        HashMap Addr (Node f) ->
        (Addr, HashMap Addr (Node f))
      ),
    Index (Stack f) ~ Int,
    IxValue (Stack f) ~ Addr,
    At (Stack f),
    Applicative f,
    Cons
      (f (Code f, Stack f))
      (f (Code f, Stack f))
      (Code f, Stack f)
      (Code f, Stack f),
    Show (f (Inst f)),
    Foldable f,
    MonadState (Machine f) m,
    MonadFail m
  )

step :: forall f m. MonadMachine f m => m ()
step =
  preuse (code % runCode % _Cons % _1) >>= \case
    Just i -> get >>= processInstr i >>= put
    _ -> "error: code: <no instructions left>" & fail
  where
    binaryOp :: forall mf. (MonadFail mf) => Inst f -> (Int -> Int -> Int) -> Machine f -> mf (Machine f)
    binaryOp i op mach =
      if
          | Just ((mach ^.) . (heap %) . at -> Just (preview _Num -> Just a), preview _Cons -> Just ((mach ^.) . (heap %) . at -> Just (preview _Num -> Just b), stack')) <- mach ^? stack % _Cons ->
            let !(!addr', !heap') = alloc @"heap" (Num $! a `op` b) (mach ^. heap)
             in return $
                  mach
                    & stack .~ (addr' :< stack')
                    & heap .~ heap'
          | otherwise -> "error: binaryOp: failed to unwind the stackfor <" <> show i <> ">" & fail

    processInstr :: forall mf. (MonadFail mf) => Inst f -> Machine f -> mf (Machine f)
    processInstr i@(Push v) mach = push v
      where
        push :: Value -> mf (Machine f)
        push (Arg ((mach ^.) . (stack %) . at . (+ 1) -> Just ((^? _Just % _App) . (mach ^.) . (heap %) . at -> Just (_, arg)))) = mach & stack %~ (arg :<) & return
        push arg@(Arg _) = "error: push: failed to get argument <" <> show i <> ">" & fail
        push (Local ((mach ^.) . (stack %) . at -> Just addr)) = mach & stack %~ (addr :<) & return
        push local@(Local _) = "error: push: failed to locate <" <> show i <> ">" & fail
        push (Global ((mach ^.) . (globals %) . at -> Just addr)) = mach & stack %~ (addr :<) & return
        push global'@(Global _) = "error: push: failed to locate <" <> show i <> ">" & fail
        push (Value v) = mach & stack %~ (addr' :<) & heap .~ heap' & return
          where
            !(!addr', !heap') = mach ^. heap & alloc @"heap" (Num v)
    processInstr i@MkApp mach =
      if
          | Just (x, preview _Cons -> Just (f, stack')) <- mach ^? stack % _Cons ->
            let !(!addr', !heap') = alloc @"heap" (App f x) (mach ^. heap)
             in mach
                  & stack .~ (addr' :< stack')
                  & heap .~ heap'
                  & return
          | otherwise -> "error: mkApp: failed to execute instruction <" <> show i <> ">" & fail
    processInstr i@(Slide n) mach =
      if
          | Just (a, stack') <- mach ^? stack % _Slide n -> mach & stack .~ (a :< stack') & return
          | otherwise -> "error: slide: failed to slide with <" <> show i <> ">" & fail
      where
        _Slide :: Int -> Prism' (Stack f) (Addr, Stack f)
        _Slide n = prism' (`intro` n) (`elim` n)
          where
            elim :: Stack f -> Int -> Maybe (Addr, Stack f)
            elim (preview _Cons -> Just (a, as)) 0 = Just (a, as)
            elim (preview _Cons -> Just (a, as)) !((`preview` as) . _Slide . (`subtract` 1) -> Just (_, as')) = Just (a, as')
            elim _ _ = Nothing

            intro :: (Addr, Stack f) -> Int -> Stack f
            intro = error "not implemented"
    processInstr i@(Cond trueB falseB) mach =
      if
          | Just ((mach ^.) . (heap %) . at -> Just (preview _Num -> Just n), stack') <- mach ^? stack % _Cons -> case n of
            1 ->
              mach
                & code %~ (Code trueB <>)
                & stack .~ stack'
                & return
            0 ->
              mach
                & code %~ (Code falseB <>)
                & stack .~ stack'
                & return
            _ -> "error: cond: branching on non-boolean <" <> show n <> ">" & fail
          | otherwise -> "error: cond: failed to branch <" <> show i <> ">" & fail
    processInstr i@Eval mach =
      if
          | Just (a, stack') <- mach ^? stack % _Cons ->
            mach
              & dump %~ ((mach ^. code, stack') :<)
              & code .~ Code (pure Unwind)
              & stack .~ Stack (pure a)
              & return
          | otherwise -> "error: eval: failed to eval top of the stack <" <> show i <> ">" & fail
    processInstr i@Unwind mach =
      if
          | Just (a, stack') <- mach ^? stack % _Cons -> case mach ^. heap % at a of
            Just (preview _Num -> Just n) -> case mach ^? dump % _Cons of
              Just ((code', stack''), dump') ->
                mach
                  & dump .~ dump'
                  & code .~ code'
                  & stack .~ (a :< stack'')
                  & return
              _ -> mach & code .~ Empty & return
            Just (preview _App -> Just (fun, _)) ->
              mach
                & stack %~ (fun :<)
                & code .~ Code (pure Unwind)
                & return
            Just (preview _Fun -> Just (name, arity, instrs))
              | 1 + ((mach ^. (stack % runStack)) & lengthOf folded) >= arity -> mach & code .~ Code instrs & return
              | otherwise -> "error: unwind: too few arguments for <" <> show (name, arity) <> ">" & fail
            _ -> "error: unwind: failed to unwind <" <> show i <> ">" & fail
          | otherwise -> "error: unwind: failed to unwind the stack <" <> show i <> ">" & fail
    processInstr i@Add mach = binaryOp i (+) mach
    processInstr i@Sub mach = binaryOp i (-) mach
    processInstr i@Mul mach = binaryOp i (*) mach
    processInstr i@Div mach = binaryOp i div mach
    processInstr i@Equ mach = binaryOp i (\a b -> if a == b then 0 else 1) mach
    processInstr i@NEq mach = binaryOp i (\a b -> if a /= b then 0 else 1) mach

type Alloc :: Symbol -> (Type -> Type) -> Type -> Constraint
class Alloc s f fnTy | s f -> fnTy where
  alloc :: fnTy

instance Alloc "heap" f (Node f -> HashMap Addr (Node f) -> (Addr, HashMap Addr (Node f))) where
  alloc n hm = (addr', hm & ix addr' .~ n)
    where
      !addr' = hm & lengthOf folded & Addr

run :: Machine f -> [Machine f]
run = error "nyi"
