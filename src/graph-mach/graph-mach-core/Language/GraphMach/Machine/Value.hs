-- |
module Language.GraphMach.Machine.Value where

data Value where
  Global :: String -> Value
  Value :: Int -> Value
  Arg :: Int -> Value
  Local :: Int -> Value
  deriving (Eq, Show)
