-- |
module Language.GraphMach.Syntax.Expr where

import Data.HashMap.Strict (HashMap)
import Optics

data Expr where
  Num :: Int -> Expr
  Var :: String -> Expr
  Let :: HashMap String Expr -> Expr
  App :: Expr -> Expr -> Expr
  deriving (Show)

data Function where
  Function ::
    { _name :: String,
      _args :: [String],
      _body :: Expr
    } ->
    Function
  deriving (Show)

makeLenses ''Function
