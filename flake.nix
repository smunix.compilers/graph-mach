{
  description = "Stack and G-machine, with graph reduction based evaluation";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/haskell-updates";
    flake-utils.url = "github:numtide/flake-utils/master";
  };
  outputs = { self, nixpkgs, flake-utils }:
    with flake-utils.lib;
    with nixpkgs.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        overlays = [
          (import ./nix/graph-mach.nix { inherit system version flake-utils; })
        ];
      in with (import nixpkgs { inherit system overlays; }); rec {
        packages =
          flattenTree (recurseIntoAttrs { inherit (libraries) graph-mach; });
        defaultPackage = packages.graph-mach;
      });
}
