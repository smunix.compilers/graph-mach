{ system, version, flake-utils, ... }:
self: _:
with self;
with haskell.lib;
with haskellPackages.extend (self: _:
  with self; rec {
    optics-core = callHackage "optics-core" "0.4" { };
    optics-extra = callHackage "optics-extra" "0.4" { inherit optics-core; };
    optics-th = callHackage "optics-th" "0.4" { inherit optics-core; };
    optics = callHackage "optics" "0.4" {
      inherit optics-core optics-extra optics-th;
    };
  }); rec {
    libraries = recurseIntoAttrs {
      graph-mach = overrideCabal (callCabal2nix "graph-mach" ../. { })
        (old: { version = "${old.version}.${version}"; });
    };
    executables = { };
  }
